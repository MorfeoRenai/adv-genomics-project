# -*- coding: utf-8 -*-
import click
import logging
from pathlib import Path
from dotenv import find_dotenv, load_dotenv

import numpy as np
import pandas as pd
from scipy import stats


def _initialize_corr_df(tf_tu_df):
    corr_df = pd.DataFrame()

    for i in tf_tu_df.index:
        row = tf_tu_df.loc[i, :]
        regulatorName = row["regulatorName"]
        activeConformation = row["activeConformation"]
        isNativeInactive = row["isNativeInactive"]
        isPhosphorylated = row["isPhosphorylated"]
        tuGenes = row["tuGenes"]
    
        tuGenes = tuGenes.split(";")
        try:
            tuGenes.remove("")
        except:
            pass
    
        # from gene name to protein name
        # suffices to uppercase the first letter
        regulatorGene = regulatorName[0].lower() + regulatorName[1:]
        
        small_df = pd.DataFrame({"regulatorName": regulatorName,
                                "activeConformation": activeConformation,
                                "isNativeInactive": isNativeInactive,
                                "isPhosphorylated": isPhosphorylated,
                                "regulatorGene": regulatorGene,
                                "targetGene": tuGenes})
        corr_df = pd.concat([corr_df, small_df])
    
    # reset index and it's ready to go
    corr_df = corr_df.reset_index(drop = True)
    
    return corr_df


def _get_spearman_corr(row, log_tpm_df):
    tf = row["regulatorGene"]
    target = row["targetGene"]
    
    # get expression profiles of TF gene and target gene
    try:
        y = log_tpm_df.loc[tf]
        x = log_tpm_df.loc[target]
    except KeyError:
        # one of the genes is not present in the logTPM matrix
        return np.nan

    # spearman rank correlation (non-parametric)
    return stats.spearmanr(x, y)[0]


def _get_spearman_pvalue(row, log_tpm_df):
    tf = row["regulatorGene"]
    target = row["targetGene"]
    
    # get expression profiles of TF gene and target gene
    try:
        y = log_tpm_df.loc[tf]
        x = log_tpm_df.loc[target]
    except KeyError:
        # one of the genes is not present in the logTPM matrix
        return np.nan

    # spearman rank correlation (non-parametric)
    return stats.spearmanr(x, y)[1]


def build_corr_df(tf_tu_df, log_tpm_df):
    # initialize dataset of correlations, without correlations for now
    # just mapping TF <--> Target Gene
    corr_df = _initialize_corr_df(tf_tu_df)

    # add correlations columns
    corr_df["spearmanCorr"] = corr_df.apply(_get_spearman_corr,
                                            axis=1,args=(log_tpm_df, ))
    corr_df["pValue"] = corr_df.apply(_get_spearman_pvalue,
                                      axis=1, args=(log_tpm_df, ))
    
    # dropping NA values in correlation column
    corr_df = corr_df.dropna(subset=["spearmanCorr"])

    # add columns for aggregate correlations
    # average of correlations, grouping by TF
    grouped_corr = corr_df.groupby(
        by=["regulatorGene"])[["spearmanCorr"]].mean()
    grouped_corr = grouped_corr.rename(
        columns={"spearmanCorr": "avgCorr"}).reset_index()
    corr_df = corr_df.merge(grouped_corr, on=["regulatorGene"])
    
    # maximum of correlations, grouping by TF
    grouped_corr = corr_df.groupby(
        by=["regulatorGene"])[["spearmanCorr"]].max()
    grouped_corr = grouped_corr.rename(
        columns={"spearmanCorr": "maxCorr"}).reset_index()
    corr_df = corr_df.merge(grouped_corr, on=["regulatorGene"])
    
    # minimum of correlations, grouping by TF
    grouped_corr = corr_df.groupby(
        by=["regulatorGene"])[["spearmanCorr"]].min()
    grouped_corr = grouped_corr.rename(
        columns={"spearmanCorr": "minCorr"}).reset_index()
    corr_df = corr_df.merge(grouped_corr, on=["regulatorGene"])
    
    return corr_df


def pipeline(input_filepath):
    # paths to input raw datasets files
    tf_tu_file = input_filepath + "/TF-TUSet.csv"
    log_tpm_file = input_filepath + "/log_tpm_norm.csv"

    # read datasets
    tf_tu_df = pd.read_csv(tf_tu_file, header=0, index_col=0)
    log_tpm_df = pd.read_csv(log_tpm_file, header=0, index_col=0)
    
    # build dataframe with correlations between TF and target gene
    corr_df = build_corr_df(tf_tu_df, log_tpm_df)
    
    return corr_df
    

@click.command()
def main():
    """
    Run data processing scripts to turn raw data from (../raw) into
    cleaned data ready to be analyzed (saved in ../processed).
    """
    input_filepath = "data/interim"
    output_filepath = "data/processed"
    
    # run pipeline
    corr_df = pipeline(input_filepath)
    
    # write datasets in output directory
    corr_df.to_csv(output_filepath + "/corr.csv")


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
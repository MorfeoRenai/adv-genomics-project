# -*- coding: utf-8 -*-
import click
import logging
from pathlib import Path
from dotenv import find_dotenv, load_dotenv

from src.data import process_raw
from src.data import process_interim

@click.command()
def main():
    """
    Runs data processing scripts to turn raw data from (../raw) into
    cleaned data ready to be analyzed (saved in ../processed).
    """

    raw_filepath = "data/raw"
    interim_filepath = "data/interim"
    processed_filepath = "data/processed"

    # run pipeline from raw to interim
    log_tpm_df, tf_tu_df, genes_df = process_raw.pipeline(raw_filepath)

    # write datasets in interim directory
    log_tpm_df.to_csv(interim_filepath + "/log_tpm_norm.csv")
    tf_tu_df.to_csv(interim_filepath + "/TF-TUSet.csv")
    genes_df.to_csv(interim_filepath + "/genesMetadata.csv")
    
    # run pipeline from interim to processed
    corr_df = process_interim.pipeline(interim_filepath)
    
    # write dataset in processed directory
    corr_df.to_csv(processed_filepath + "/corr.csv")


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()

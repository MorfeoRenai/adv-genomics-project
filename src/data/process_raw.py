# -*- coding: utf-8 -*-
import click
import logging
from pathlib import Path
from dotenv import find_dotenv, load_dotenv

import re
import numpy as np
import pandas as pd
from UniProtMapper import ProtMapper


def _extract_TU(s):
    if s is np.nan:
        return s
    tu = s.split(":")[0]
    return tu


def request_gene_data(gene_names):
    # request a gene names gene_data dataset
    # uses the UniProt API, querying gene data for gene names
    # the thing of most interest are the gene name synonyms
    # since logTPM has weird gene names ex: b0002
    # we need to rename them in order to join logTPM matrix with data
    # about the TF-targets interaction
    print("Making a request for gene names synonyms to UniProt database...")
    mapper = ProtMapper()
    gene_data_df, failed = mapper.get(ids=gene_names,
                                      from_db="Gene_Name",
                                      to_db="UniProtKB")
    print("Number of failed gene name synonyms requests:",
          len(failed))

    # addressing duplicates
    gene_data_df = gene_data_df.drop_duplicates()

    return gene_data_df

def process_tf(tf_df):
    # select only useful columns
    tf_df = tf_df.loc[:, ['1)riId', '2)riType', '3)regulatorId',
                          '4)regulatorName', '5)cnfName', '11)riFunction',
                          '19)targetTuOrGene']]
    
    # renaming columns
    tf_df.columns = ['riId', 'riType', 'regulatorId', 'regulatorName',
                     'activeConformation', 'riFunction', 'targetTuOrGene']

    # select Transcription Factors only
    tf_df = tf_df[tf_df["riType"].isin(["tf-promoter", "tf-tu", "tf-gene"])]

    # select activators only
    tf_df = tf_df[tf_df["riFunction"] == "activator"]
    tf_df = tf_df.drop("riFunction", axis=1)

    # extract Transcription Unit ID from targetTuOrGene column
    # ex: RDBECOLITUC03315:hdeAB-yhiD -> RDBECOLITUC03315
    tf_df["tuId"] = tf_df["targetTuOrGene"].apply(_extract_TU)

    # make a flag for active configurations is different from the
    # configurations. This happens in 3 cases:
    #   - Post-Translation Modification
    #   - binding ligand
    #   - binding another protein
    tf_df["isNativeInactive"] = tf_df[
        'regulatorName'] != tf_df['activeConformation']
    
    # make flag for phosphorylated TF
    # phosphorylation is a common Post-Translation Modification
    tf_df["isPhosphorylated"] = tf_df.apply(_get_phosphorilation, axis=1)

    # addressing duplicates
    tf_df = tf_df.drop_duplicates()

    return tf_df


def process_tu(tu_df):
    # select only useful columns
    tu_df = tu_df.loc[:, ['1)tuId', '2)tuName', '3)operonName', '4)tuGenes']]
    
    # renaming columns
    tu_df.columns = ['tuId', 'tuName', 'operonName', 'tuGenes']

    # addressing duplicates
    tu_df = tu_df.drop_duplicates()

    return tu_df


def process_log_tpm(log_tpm_df):
    # renaming first column (unnamed) to "b_id" which is a special gene name type
    log_tpm_df = log_tpm_df.rename(columns={"Unnamed: 0": "bId"})

    # addressing duplicates
    log_tpm_df = log_tpm_df.drop_duplicates()

    return log_tpm_df


def process_gene_data(genes_df):
    # renaming first column (From) to "b_id" which is a special gene name type
    genes_df = genes_df.rename(columns={"From": "bId"})

    # new column with only the first gene name synonym
    # the other synonyms are not needed
    genes_df["First Gene Name"] = genes_df["Gene Names"].apply(
        lambda s: s.split(" ")[0])
    
    # moving column
    column = genes_df.pop("First Gene Name")
    genes_df.insert(3, "First Gene Name", column)
    
    # filter only E. coli genes
    genes_df = genes_df[genes_df["Organism"] ==
                                'Escherichia coli (strain K12)']
    
    # addressing duplicates
    genes_df = genes_df.drop_duplicates()
    genes_df = genes_df.drop_duplicates(subset=["bId"],
                                        keep="first")
    genes_df = genes_df.drop_duplicates(subset=["First Gene Name"],
                                        keep="first")

    return genes_df


def _get_phosphorilation(row):
    activeConformation = row["activeConformation"]
    isNativeInactive = row["isNativeInactive"]
    
    if not isNativeInactive:
        return False
    elif re.search("[Pp]hosphorylated", activeConformation):
        return True
    else:
        return False


# def get_kinase(row):
#     ptm = row["PTM"]

#     # the text in the parenthesis (....) is the kinase gene name
#     pattern = "[Pp]hosphorylated.*by (....)"
#     search = re.search(pattern, ptm)

#     if search:
#         kinase = search[1]
#         kinase_gene = kinase[0].lower() + kinase[1:]
#         return kinase_gene
#     else:
#         return np.nan


def pipeline(input_filepath):
    # paths to input raw datasets files
    tf_file = input_filepath + "/TF-RISet.tsv"
    tu_file = input_filepath + "/TUSet.tsv"
    log_tpm_file = input_filepath + "/log_tpm_norm.csv"

    # read datasets
    tf_df = pd.read_csv(tf_file, delimiter="\t")
    tu_df = pd.read_csv(tu_file, delimiter="\t")
    log_tpm_df = pd.read_csv(log_tpm_file)

    # process datasets
    tf_df = process_tf(tf_df)
    tu_df = process_tu(tu_df)
    log_tpm_df = process_log_tpm(log_tpm_df)

    # inner join of two datasets:
    # Transcriptional Factor <---> Transcriptional Unit
    tf_tu_df = tf_df.merge(tu_df, on='tuId', how='inner')

    # request gene data, by using the gene names of type bId
    # bId gene names are used in the logTPM matrix as row names
    # remember that gene_names must be in a list data structure
    gene_names = log_tpm_df["bId"].to_list()
    genes_df = request_gene_data(gene_names)

    # process gene metadata
    genes_df = process_gene_data(genes_df)
    
    # ok now let's make the datasets coherent thru joining
    temp = pd.merge(genes_df, log_tpm_df, on="bId")
    genes_df = temp.loc[:, "bId":"Sequence"]
    genes_df = genes_df.set_index("First Gene Name")
    log_tpm_df = temp.loc[:, 'control__wt_glc__1':]
    log_tpm_df.index = genes_df.index
    
    return (log_tpm_df, tf_tu_df, genes_df)


@click.command()
def main():
    """
    Run data processing scripts to turn raw data from (../raw) into
    cleaned data ready to be analyzed (saved in ../processed).
    """
    input_filepath = "data/raw"
    output_filepath = "data/interim"
    
    # run pipeline
    log_tpm_df, tf_tu_df, genes_df = pipeline(input_filepath)
    
    # write datasets in output directory
    log_tpm_df.to_csv(output_filepath + "/log_tpm_norm.csv")
    tf_tu_df.to_csv(output_filepath + "/TF-TUSet.csv")
    genes_df.to_csv(output_filepath + "/genesMetadata.csv")


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
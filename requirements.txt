# local package
-e .

# external requirements
click
Sphinx
coverage
flake8
python-dotenv>=0.5.1

# model and visualization
seaborn
matplotlib
scikit-learn
scipy
pandas
numpy

# requests using UniProt API
uniprot-id-mapper

from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='A project regarding the Advanced Genomics course. The goal is to study co-expression of transcription factors and their targets in E. coli and their activity proxy functions.',
    author='Alessandro Pandolfi',
    license='MIT',
)
